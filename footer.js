document.addEventListener('DOMContentLoaded', function() {
    var pageTop = document.getElementById("page-top");
    if (pageTop !== null) {

        // get all listitems
        var titleTileLists = document.querySelectorAll(".tile-title");



        

        var changeModalboxTitle = function(){
            var postMetadataTitle = document.getElementById("postMetadataTitle");
            postMetadataTitle.style.display = "none";
            setTimeout(() => {
                var modalBoxTitle = document.querySelector(".post-details-title");  
                var newModalTitle = modalBoxTitle.innerHTML.replace('SAST - ','SAST<br>');  
                modalBoxTitle.innerHTML = newModalTitle;
                postMetadataTitle.style.display = "block";
            }, 400);
     
            // alert(modalBoxTitle.innerHTML);      

        }

        for (var i = 0; i < titleTileLists.length; i++) {
            titleTileLists[i].addEventListener('click', changeModalboxTitle, false);
        }
        
    }
});
